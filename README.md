# iPhoneApp AAAA / ああああ

<br>

![](AAAA/Assets.xcassets/LaunchIcon.imageset/AAAA_LaunchImage.png)

<br>

## 🔗 AppStore link

[apps.apple.com/app/id1111](https://apps.apple.com/app/id1111)

<!-- Manually sync below text between "🛠Menu.swift/📄About" and "/README.md(here)" and "AppStoreConnect/AAAA/Description". -->

## 📄 Description

<!--==== English Description ====-->

AAAA.

【Target/Use-case】

- AAAA

【OPTION】

- AAAA

【OTHERS】

- AAAA


==== Japanese(native) Description ====

ああああ。

【想定ユーザー/ユースケース】

- ああああ

【オプション】

- ああああ

【その他】

- ああああ


==== Localization: All ====

English

==== Localization: Almost all ====

Japanese(native)

==== Localization: Main 3 keywords only ====

(English: )

Simplified_Chinese: 

Traditional_Chinese: 

Spanish: 

Portuguese: 

Russian: 

Indonesian: 

French:  

Arabic:  () /  () / ()

German: 

Korean: 

<br>


## 🧰 Source code link

[github.com/FlipByBlink/AAAA](https://github.com/FlipByBlink/AAAA)


## ✉️ Contact

sear_pandora_0x@icloud.com




<br>

<br>

------

<br>

<br>


## Privacy Policy for AppStore


2022-??-??


### Japanese

このアプリ自身において、ユーザーの情報を一切収集しません。


### English

This application don't collect user infomation.


<br>

<br>

------

<br>

<br>


<!-- URL "Support page for AppStore" -->
<!-- https://flipbyblink.github.io/AAAA/ -->

<!-- URL "Privacy Policy for AppStore" -->
<!-- https://github.com/FlipByBlink/AAAA#privacy-policy-for-appstore -->
